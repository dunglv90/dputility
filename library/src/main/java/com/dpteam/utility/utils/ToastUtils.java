package com.dpteam.utility.utils;

import android.app.Activity;
import android.widget.Toast;

/**
 * Created by Dung on 16/10/2014.
 */
public class ToastUtils {
    public static void showToast(final Activity activity, final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
