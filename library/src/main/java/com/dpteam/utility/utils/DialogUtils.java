package com.dpteam.utility.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;

import com.dpteam.utility.R;

/**
 * Created by Dung on 3/10/2014.
 */
public class DialogUtils {
    /**
     * Create progress dialog
     */
    public static ProgressDialog createProgressDialog(Activity context) {
        ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(context.getString(R.string.loading));
        mProgressDialog.setCancelable(true);
        return mProgressDialog;
    }

    /**
     * Show alert dialog
     *
     * @param context
     * @param message
     */
    public static void showAlert(final Activity context, final String message) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(context)
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton("OK", null)
                        .show();
            }
        });

    }

}
