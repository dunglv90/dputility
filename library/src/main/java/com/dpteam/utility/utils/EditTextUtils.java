package com.dpteam.utility.utils;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by dunglv on 2/6/15.
 */
public class EditTextUtils {
    /**
     * Hide soft keyboard with editText
     *
     * @param activity
     * @param editText
     */
    public static void hideSoftKeyboard(Activity activity, EditText editText) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    /**
     * Hide soft keyboard
     *
     * @param activity
     */
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
    }

    /**
     * Check valid email edit text
     *
     * @param editText
     * @return
     */
    public static boolean isValidEmail(EditText editText) {
        return !TextUtils.isEmpty(editText.getText().toString()) &&
                android.util.Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString()).matches();
    }
}
