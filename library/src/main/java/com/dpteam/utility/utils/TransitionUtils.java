package com.dpteam.utility.utils;

import android.app.Activity;
import android.content.Intent;

import com.dpteam.utility.R;

/**
 * Created by kingfisher on 7/1/15.
 */
public class TransitionUtils {
    /**
     * Start activity which the in-activity will be slided in from left to right
     * and the out-activity will be slided out from left to right too
     *
     * @param outAct     {@link Activity} out-acitivty
     * @param intent     {@link Intent} in-activity
     * @param isFinished {@link Boolean} is finished after start new activity or not
     */
    public static void startActivitySlidingLeftToRight(Activity outAct,
                                                       Intent intent, boolean isFinished) {
        outAct.startActivity(intent);
        outAct.overridePendingTransition(R.anim.slide_in_left_to_right,
                R.anim.slide_out_left_to_right);
        if (isFinished) {
            outAct.finish();
        }
    }

    /**
     * Finish activity with animation move from left to right
     *
     * @param outAct
     * @param isFinished
     */
    public static void finishActivitySlidingRightToLeft(Activity outAct, boolean isFinished) {
        if (isFinished) outAct.finish();
        outAct.overridePendingTransition(R.anim.slide_in_right_to_left, R.anim.slide_out_right_to_left);
    }


    /**
     * Start activity which the in-activity will be slided in from right to left
     * and the out-activity will be slided out from right to left too
     *
     * @param outAct     {@link Activity} out-acitivty
     * @param intent     {@link Intent} in-activity
     * @param isFinished {@link Boolean} is finished after start new activity or not
     */
    public static void startActivitySlidingRightToLeft(Activity outAct,
                                                       Intent intent, boolean isFinished) {
        outAct.startActivity(intent);
        outAct.overridePendingTransition(R.anim.slide_in_right_to_left,
                R.anim.slide_out_right_to_left);
        if (isFinished) {
            outAct.finish();
        }
    }

    /**
     * Finish activity with animation move from left to right
     *
     * @param outAct
     * @param isFinished
     */
    public static void finishActivitySlidingLeftToRight(Activity outAct, boolean isFinished) {
        if (isFinished) outAct.finish();
        outAct.overridePendingTransition(com.dpteam.utility.R.anim.slide_in_left_to_right, com.dpteam.utility.R.anim.slide_out_left_to_right);
    }
}
