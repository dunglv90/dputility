package com.dpteam.utility.utils;

import android.util.Base64;

/**
 * Created by kingfisher on 10/15/15.
 */
public class EncodeHelper {
    /**
     * Swap character between odd and even position in list
     *
     * @param key
     * @return
     */
    public static String swapChars(String key) {
        StringBuilder stringBuilder = new StringBuilder(key);
        int length = key.length();
        for (int i = 0; i < key.length(); i += 2) {
            if (i + 1 >= key.length()) {
                break;
            }
            char store = stringBuilder.charAt(i);
            stringBuilder.setCharAt(i, stringBuilder.charAt(i + 1));
            stringBuilder.setCharAt(i + 1, store);
        }
        return stringBuilder.toString();
    }

    /**
     * Decode an encoded Url to normal Url
     *
     * @param encodedUrl
     * @return
     */
    public static String decodeUrl(String encodedUrl) {
        return new String(Base64.decode(encodedUrl, Base64.DEFAULT));
    }

    /**
     * Encode an url as base 64.
     *
     * @param url
     * @return
     */
    public static String encodeUrl(String url) {
        return new String(Base64.encode(url.getBytes(), Base64.DEFAULT));
    }
}
