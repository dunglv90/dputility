package com.dpteam.utility.utils;

import java.util.List;

public class ListUtils {

    /**
     * Checks if {@link java.util.List} is null or empty.
     *
     * @param <E>  the generic type
     * @param list the list
     * @return true, if is null or empty
     */
    public static <E> boolean isNullOrEmpty(List<E> list) {
        return list == null || list.size() == 0;
    }

    /**
     * Checks if {@link java.util.List} is not null and empty.
     *
     * @param <E>  the generic type
     * @param list the list
     * @return true, if is not null and empty
     */
    public static <E> boolean isNotNullAndEmpty(List<E> list) {
        return list != null && list.size() != 0;
    }
}