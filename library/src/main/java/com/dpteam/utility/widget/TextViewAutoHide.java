package com.dpteam.utility.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

/**
 * This Textview will show text then after a short time will be hiden.
 *
 * @author kingfisher.phuoc
 */
public class TextViewAutoHide extends TextView {

    public TextViewAutoHide(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public TextViewAutoHide(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public TextViewAutoHide(Context context) {
        super(context);
    }

    private static final int TIME_AUTO_HIDE = 400; // 3s then hide.
    private Handler mHandlerAutoHide = new Handler();
    private Runnable mRunnableAutoHide = new Runnable() {

        @Override
        public void run() {

            AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.0f);
            fadeOut.setDuration(TIME_AUTO_HIDE);
            TextViewAutoHide.this.startAnimation(fadeOut);
            TextViewAutoHide.this.setVisibility(View.GONE);
        }
    };

    /**
     * Show information text then hide it after {@value #TIME_AUTO_HIDE}
     *
     * @param text
     */
    public void showText(String text) {

        mHandlerAutoHide.removeCallbacksAndMessages(null);
        if (getVisibility() != VISIBLE) {
            this.setVisibility(View.VISIBLE);
        }
        this.setText(text);
        mHandlerAutoHide.postDelayed(mRunnableAutoHide, TIME_AUTO_HIDE);
    }
}
