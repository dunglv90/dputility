package com.dpteam.utility.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class ServiceHelper {


    /**
     * Get information from API Url.
     *
     * @param url
     * @return {@link String} or <code>null</code>
     * @throws Exception
     */
    public static String get(String url) throws Exception {
        // Get the Json response
        InputStream response = new URL(url).openStream();
        return convertStreamToString(response);
    }


    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
