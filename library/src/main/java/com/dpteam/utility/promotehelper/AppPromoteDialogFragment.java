package com.dpteam.utility.promotehelper;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dpteam.utility.R;
import com.dpteam.utility.widget.MultiViewPager;

import java.util.List;

public class AppPromoteDialogFragment extends DialogFragment implements View.OnClickListener {

    private List<AppPromoteHelper.AppPromote> list;
    private AppPromoteAdapter adapter;
    //    private View btnOpen;
    private TextView btnExit;
    private TextView btnMinimize;
    private TextView btnCancel;
    private FragmentActivity activity;
    private int colorResId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.dialog_promote_app, container, false);

        // Setup Multi-Viewpager
        MultiViewPager multiViewPager = (MultiViewPager) view.findViewById(R.id.pager);
        list = AppPromoteHelper.getInstances(getActivity()).getListPromotedApps();
        if (adapter == null) {
            adapter = new AppPromoteAdapter(getChildFragmentManager(), list, colorResId);
        }
        multiViewPager.setAdapter(adapter);

        // Setup Open, close Button
//        btnOpen = view.findViewById(R.id.btnOpen);
        btnCancel = (TextView) view.findViewById(R.id.btnDismiss);
        btnExit = (TextView) view.findViewById(R.id.btnExit);
        btnMinimize = (TextView) view.findViewById(R.id.btnMinimize);
        setButtonColor(colorResId);

//        btnOpen.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnExit.setOnClickListener(this);
        btnMinimize.setOnClickListener(this);

//        btnOpen.setVisibility(isExitDialog ? View.GONE : View.VISIBLE); // show in more dialog
        btnExit.setVisibility(isExitDialog ? View.VISIBLE : View.GONE); // show in exit dialog
        btnMinimize.setVisibility(isShowMinimize ? View.VISIBLE : View.GONE); // show in exit dialog

        return view;
    }

    public static AppPromoteDialogFragment showPromoteDialog(FragmentActivity activity, int colorResId, boolean isExitDialog, boolean isShowMinimize, AppPromoteHelper.OnPromoteDialogClickListener listener) {
        AppPromoteDialogFragment appPromoteDialogFragment = new AppPromoteDialogFragment();
        appPromoteDialogFragment.activity = activity;
        appPromoteDialogFragment.isExitDialog = isExitDialog;
        appPromoteDialogFragment.isShowMinimize = isShowMinimize;
        appPromoteDialogFragment.mListener = listener;
        appPromoteDialogFragment.colorResId = colorResId;
        appPromoteDialogFragment.show(activity.getSupportFragmentManager(), null);
        appPromoteDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return appPromoteDialogFragment;
    }


    boolean isExitDialog = false;
    boolean isShowMinimize = false;
    AppPromoteHelper.OnPromoteDialogClickListener mListener;


    /**
     * Must call in {@link #onResume()} to change size of promote dialog
     */
    private void setupSizeOfPromoteDialog() {
        // change dialog size
        int dialogWidth = getActivity().getResources().getDimensionPixelOffset(R.dimen.margin_360);

        int dialogHeight = (int) (dialogWidth * 1.2f);

        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupSizeOfPromoteDialog();
    }


    @Override
    public void onClick(View v) {
        try {
            if (v.getId() == btnMinimize.getId()) {
                if (mListener != null)
                    mListener.OnMinimizeClicked();
                dismiss();
            } else if (v.getId() == btnExit.getId()) {
                if (mListener != null)
                    mListener.OnExitClicked();
                dismiss();
            } else if (v.getId() == btnCancel.getId()) {
                if (mListener != null)
                    mListener.OnDismissClicked();
                dismiss();
            }
        } catch (Exception e) {
            //Avoid Dissmiss error reported from Google Play Store
            e.printStackTrace();
        }
    }

    private void setButtonColor(int colorResId) {
        if (colorResId != 0) {
            btnExit.setTextColor(activity.getResources().getColor(colorResId));
            btnMinimize.setTextColor(activity.getResources().getColor(colorResId));
            btnCancel.setTextColor(activity.getResources().getColor(colorResId));
        }
    }

}
