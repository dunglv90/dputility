package com.dpteam.utility.promotehelper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * <pre>
 * <b>How to use:</b>
 * 1. Need 2 permission: "Read external storage" and "Internet"
 * 2. Must call {@link #loadRemoteConfigOffUiThread()} in 1st activity of application.
 * 3. (optional) Can change "Promote link" by function {@link #setPromoteUrl(String)} rather then using default link
 * 4. (optional) Can show promote dialog with function: {@link #showPromoteDialog(android.support.v4.app.FragmentActivity, com.dpteam.utility.promotehelper.AppPromoteHelper.OnPromoteDialogClickListener)}
 * </pre>
 * <pre>
 * <b>How to config:</b>
 * 1. title : title of app <b>(required)</b>
 * 2. packageName: package name of app in playstore <b>(required)</b>
 * 3. developer: the account of app to show all apps <b>(required)</b>
 * 4. message: message to explain what app does <b>(required)</b>
 * 5. iconApp: app icon in playstore <b>(required)</b>
 * 6. banner_image_url: app feature image <b>(required)</b>
 * 7. exceptCountries: list of ISO 3166-1 alpha-2  country codes to hide promote in those countries
 *      example: exceptCountries:"vn,gb,usa" (Optional)
 * 8. countries: list of ISO 3166-1 alpha-2 country codes to only show promote in this country
 *      example: countries:"vn,usa" (Optional)
 * </pre>
 */
public class AppPromoteHelper {

    /**
     * Service link
     */
    private String promote_url = "http://dpteams.com/promote/app_promote.txt";
    private static final String ADS_PREFERENCES_NAME = "AdsPreferences";
    private static final String KEY_JSON = "jsonKey";
    private static final String KEY_LAST_UPDATE_CONFIG = "lastUpdateConfig";
    private static final long ONE_DAY_IN_MILLISECONDS = 24 * 3600000;

    private static AppPromoteHelper mAppPromoteHelper;
    private SharedPreferences mAdsPreferences;

    private boolean isSuggestedAppShow = false;
    private List<AppPromote> appPromotes = new ArrayList<>();
    private Context mContext;

    private boolean isShowMinimize;
    private boolean isExitDialog;
    private int colorResId;

    private AppPromoteHelper(Context context) {
        mAdsPreferences = context.getSharedPreferences(ADS_PREFERENCES_NAME, Context.MODE_PRIVATE);
        mContext = context.getApplicationContext();
        loadPromoteSettings();
        initImageLoader();
    }

    /**
     * Init image loader library to load image from remote url efficiently
     */
    private void initImageLoader() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                .denyCacheImageMultipleSizesInMemory()
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .defaultDisplayImageOptions(defaultOptions) // Default
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }


    /**
     * Load promote settings and remove uninstalled app.
     */
    private void loadPromoteSettings() {
        PromoteSettings promoteSettings = getPromoteSettings();
        if (promoteSettings != null && promoteSettings.listPromoteApps != null
                && !promoteSettings.listPromoteApps.isEmpty()) {
            String countryCode = getUserCountry(mContext);
            appPromotes.clear();
            for (AppPromote appPromote : promoteSettings.listPromoteApps) {
                // Is app installed, dont add
                if (isPackageInstalled(appPromote.packageName, mContext)) {
                    continue;
                }
                // if country code not in show list, dont add
                if (!TextUtils.isEmpty(appPromote.countries) && !TextUtils.isEmpty(countryCode)
                        && !appPromote.countries.toLowerCase().contains(countryCode)) {
                    continue;
                }
                // if country code is in exception list, dont add
                if (!TextUtils.isEmpty(appPromote.exceptContries) && !TextUtils.isEmpty(countryCode)
                        && appPromote.exceptContries.toLowerCase().contains(countryCode)) {
                    continue;
                }
                appPromotes.add(appPromote);
            }
            isSuggestedAppShow = promoteSettings.isAdsShow && !appPromotes.isEmpty();
        } else {
            isSuggestedAppShow = false;
            appPromotes.clear();
        }
    }

    /**
     * Is should show promote app or not.
     *
     * @return
     */
    public boolean isSuggestedAppsShow() {
        return this.isSuggestedAppShow;
    }

    public List<AppPromote> getListPromotedApps() {
        return this.appPromotes;
    }

    public AppPromoteHelper setPromoteUrl(String promoteUrl) {
        this.promote_url = promoteUrl;
        return this;
    }

    public static AppPromoteHelper getInstances(Context context) {
        if (mAppPromoteHelper == null) {
            mAppPromoteHelper = new AppPromoteHelper(context);
        }
        return mAppPromoteHelper;
    }

    /**
     * Get Http String from remote Url.
     *
     * @param url
     * @return
     */
    private String getHttp(String url) {
        try {
            return convertStreamToString(new URL(url).openStream());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    /**
     * Should update config today?
     *
     * @return
     */
    private boolean shouldUpdateConfig() {
        long lastUpdate = mAdsPreferences.getLong(KEY_LAST_UPDATE_CONFIG, 0l);
        if (lastUpdate == 0 || System.currentTimeMillis() - lastUpdate > ONE_DAY_IN_MILLISECONDS) {
            return true;
        }
        return false;
    }

    /**
     * Load remote config from Server off UI thread to avoid ANR
     */
    public void loadRemoteConfigOffUiThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                loadRemoteConfig();
            }
        }).start();
    }

    /**
     * Load remote config from remote server to local db.
     */
    private void loadRemoteConfig() {
        if (shouldUpdateConfig()) { // reload json from server after 1 day
            String json = getHttp(promote_url);
            if (!TextUtils.isEmpty(json)) {
                mAdsPreferences.edit().putString(KEY_JSON, json).commit();
                mAdsPreferences.edit().putLong(KEY_LAST_UPDATE_CONFIG, System.currentTimeMillis()).commit();
                loadPromoteSettings();
            }
        } else { // reload installed app
            loadPromoteSettings();
        }
    }

    /**
     * Check is application installed or not
     *
     * @param packageName
     * @param context
     * @return
     */
    private boolean isPackageInstalled(String packageName, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * Get ISO 3166-1 alpha-2 country code for this device (or null if not available)
     *
     * @param context Context reference to get the TelephonyManager instance from
     * @return lower case country code or null
     */
    private static String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Get promote settings to show to UI
     *
     * @return
     */
    private PromoteSettings getPromoteSettings() {
        String json = mAdsPreferences.getString(KEY_JSON, "");
        if (!TextUtils.isEmpty(json)) {
            try {
                return new Gson().fromJson(json, PromoteSettings.class);
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    private WeakReference<AppPromoteDialogFragment> dialogFragmentWeakReference;

    public void showPromoteDialog(final FragmentActivity activity, final OnPromoteDialogClickListener listener) {
        if (isSuggestedAppShow) {
            AppPromoteDialogFragment appPromoteDialogFragment = AppPromoteDialogFragment.showPromoteDialog(activity,
                    colorResId, isExitDialog, isShowMinimize, listener);
            dialogFragmentWeakReference = new WeakReference<AppPromoteDialogFragment>(appPromoteDialogFragment);
        }
    }

    /**
     * Dismiss promote dialog if need
     */
    public void dissmissPromoteDialog() {
        if (dialogFragmentWeakReference != null && dialogFragmentWeakReference.get() != null) {
            dialogFragmentWeakReference.get().dismiss();
            dialogFragmentWeakReference = null;
        }
    }


    public static class PromoteSettings {
        @SerializedName ("isAdsShow")
        public Boolean isAdsShow = false;
        @SerializedName ("list_apps")
        public List<AppPromote> listPromoteApps;

    }

    public static class AppPromote {
        @SerializedName ("title")
        public String title;
        @SerializedName ("packageName")
        public String packageName;
        @SerializedName ("developer")
        public String developer;
        @SerializedName ("message")
        public String message;
        @SerializedName ("iconApp")
        public String iconApp;
        @SerializedName ("countries")
        public String countries;
        @SerializedName ("exceptCountries")
        public String exceptContries;
        @SerializedName ("redirect_link")
        public String redirect_url;
        @SerializedName ("banner_image_url")
        public String banner_image_url;

        @Override
        public String toString() {
            return "AppPromote{" +
                    "title='" + title + '\'' +
                    ", packageName='" + packageName + '\'' +
                    ", developer='" + developer + '\'' +
                    ", message='" + message + '\'' +
                    ", iconApp='" + iconApp + '\'' +
                    ", countries='" + countries + '\'' +
                    ", exceptContries='" + exceptContries + '\'' +
                    ", redirect_url='" + redirect_url + '\'' +
                    ", banner_image_url='" + banner_image_url + '\'' +
                    '}';
        }
    }


    public static abstract class OnPromoteDialogClickListener {
        public void OnExitClicked() {
        }

        public void OnMinimizeClicked() {
        }

        public void OnDismissClicked() {
        }
    }

    public AppPromoteHelper setShowMinimize(boolean isShowMinimize) {
        this.isShowMinimize = isShowMinimize;
        return this;
    }

    public AppPromoteHelper setColorResId(int colorResId) {
        this.colorResId = colorResId;
        return this;
    }

    public AppPromoteHelper setExitDialog(boolean isExitDialog) {
        this.isExitDialog = isExitDialog;
        return this;
    }
}
