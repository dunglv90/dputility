package com.dpteam.utility.promotehelper;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dpteam.utility.R;
import com.dpteam.utility.fragment.BaseFragment;
import com.dpteam.utility.widget.FButton;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by kingfisher on 4/9/15.
 */
public class AppPromoteFragment extends BaseFragment implements View.OnClickListener {


    @Override
    protected int getLayoutId() {
        return R.layout.item_dialog_promote;
    }

    private ImageView ivIcon;
    private TextView tvTitle;
    private TextView tvDeveloper;
    private TextView tvMessage;
    private FButton btnInstall;
    private ImageView mBannerImage;
    private View appContentView;
    private int colorResId; // Theme color

    private AppPromoteHelper.AppPromote appPromote;

    public void setAppPromote(AppPromoteHelper.AppPromote appPromote) {
        this.appPromote = appPromote;
    }

    public static AppPromoteFragment newInstance(AppPromoteHelper.AppPromote appPromote, int colorResId) {
        AppPromoteFragment appPromoteFragment = new AppPromoteFragment();
        appPromoteFragment.setAppPromote(appPromote);
        appPromoteFragment.colorResId = colorResId;
        return appPromoteFragment;
    }


    @Override
    protected void initializeViews(View view) {
        ivIcon = (ImageView) view.findViewById(R.id.appIcon);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        tvDeveloper = (TextView) view.findViewById(R.id.tvDeveloperName);
        tvMessage = (TextView) view.findViewById(R.id.tvMessage);
        btnInstall = (FButton) view.findViewById(R.id.btnInstall);
        mBannerImage = (ImageView) view.findViewById(R.id.feature_image);
        appContentView = view.findViewById(R.id.app_content);

        btnInstall.setOnClickListener(this);
        ivIcon.setOnClickListener(this);
        tvTitle.setOnClickListener(this);
        tvDeveloper.setOnClickListener(this);
        appContentView.setOnClickListener(this);

        setData();
    }

    private void setData() {
        try {
            tvTitle.setText(appPromote.title);
            tvDeveloper.setText(appPromote.developer);
            tvMessage.setText(appPromote.message);
            if (colorResId != 0) {
                btnInstall.setButtonColor(ContextCompat.getColor(context, colorResId));
            }
            ImageLoader.getInstance().displayImage(appPromote.iconApp, ivIcon);
            ImageLoader.getInstance().displayImage(appPromote.banner_image_url, mBannerImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void setContentForView(View view) {

    }

    @Override
    public void onClick(View view) {
        try {
            int id = view.getId();
            if (id == ivIcon.getId() || tvTitle.getId() == id || btnInstall.getId() == id || appContentView.getId() == id) {
                // user tap Icon, Promote image, Title, Or install button -> launch Market
                MarketHelper.openAppInMarket(context, appPromote.packageName);
            } else if (id == tvDeveloper.getId()) { // user tap in developer text
                MarketHelper.openDeveloperInGoogleMarket(context, appPromote.developer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
