package com.dpteam.utility.promotehelper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by kingfisher on 1/14/15.
 */
public class AppPromoteAdapter extends FragmentStatePagerAdapter {
    private List<AppPromoteHelper.AppPromote> appPromotes;
    private int colorResId; // "Install" button color

    public AppPromoteAdapter(FragmentManager fm, List<AppPromoteHelper.AppPromote> list, int colorResId) {
        super(fm);
        this.appPromotes = list;
        this.colorResId = colorResId;
    }


    @Override
    public Fragment getItem(int position) {
        if (position >= appPromotes.size()) {
            return null;
        }
        return AppPromoteFragment.newInstance(appPromotes.get(position), colorResId);
    }

    @Override
    public int getCount() {
        return appPromotes == null ? 0 : appPromotes.size();
    }
}

//public class AppPromoteAdapter extends BaseArrayAdapter<AppPromoteHelper.AppPromote> {
//
//
//    public AppPromoteAdapter(Activity context, List<AppPromoteHelper.AppPromote> objects) {
//        super(context, R.layout.item_dialog_promote, objects);
//    }
//
//    private class ViewHolder implements ViewHolderBase {
//
//
//        private ImageView imageView;
//        private TextView tvTitle;
//        private TextView tvDeveloper;
//        private TextView tvMessage;
//
//        @Override
//        public void findViews(View view) {
//            imageView = (ImageView) view.findViewById(R.id.appIcon);
//            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
//            tvDeveloper = (TextView) view.findViewById(R.id.tvDeveloperName);
//            tvMessage = (TextView) view.findViewById(R.id.tvMessage);
//            tvDeveloper.setVisibility(View.GONE); // dont show this information.
//        }
//
//        @Override
//        public void setData(int position) {
//            AppPromoteHelper.AppPromote appPromote = getItem(position);
//            tvTitle.setText(appPromote.title);
//            tvDeveloper.setText(appPromote.developer);
//            tvMessage.setText(appPromote.message);
//            ImageLoader.getInstance().displayImage(appPromote.iconApp, imageView);
//        }
//    }
//
//    @Override
//    protected ViewHolderBase getViewHolder() {
//        return new ViewHolder();
//    }
//
//}
