package com.dpteam.utility.promotehelper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by kingfisher on 12/8/14.
 */
public class MarketHelper {
    private static final MARKET market = MARKET.PLAYSTORE;

    /**
     * Open developer account in google play store.
     *
     * @param activity
     * @param developerId
     */
    public static void openDeveloperInGoogleMarket(Activity activity,
                                                   String developerId) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("market://developer?id=" + developerId)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("https://play.google.com/store/apps/developer?id="
                            + developerId)));
        }
    }

    /**
     * Open app in each store.
     *
     * @param activity
     */
    public static void openAppInMarket(Activity activity, String appPackageName) {
        switch (market) {
            case AMAZONSTORE:
                openAppInAmazonMarket(activity, appPackageName);
                break;
            case PLAYSTORE:
                openAppInGoolgeMarket(activity, appPackageName);
                break;
            case APPSTOREVN:

                break;
            default:
                break;
        }
    }

    /**
     * Open app in google play store for rating??
     *
     * @param activity
     */
    private static void openAppInGoolgeMarket(Activity activity,
                                              String appPackageName) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("http://play.google.com/store/apps/details?id="
                            + appPackageName)));
        }
    }

    /**
     * Link to amazon store.
     *
     * @param activity
     */
    private static void openAppInAmazonMarket(Activity activity,
                                              String appPackageName) {
        Intent goToAppstore = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.amazon.com/gp/mas/dl/android?p="
                        + appPackageName));
        goToAppstore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(goToAppstore);
    }

    private enum MARKET {
        PLAYSTORE, AMAZONSTORE, APPSTOREVN
    }
}
