package com.dpteam.utility.adshelper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kingfisher on 4/6/15.
 */
public class AdConfigPreferences {

    private final String AdsConfigName = "AdsConfig";
    private SharedPreferences sharedPreferences;
    private final String KEY_ADS_NUMBER = "adsNumber";

    private static AdConfigPreferences adConfigPreferences;

    public static AdConfigPreferences getInstance(Context context) {
        if (adConfigPreferences == null) {
            adConfigPreferences = new AdConfigPreferences(context.getApplicationContext());
        }
        return adConfigPreferences;
    }

    private AdConfigPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences(AdsConfigName, Context.MODE_PRIVATE);
    }

    /**
     * Get ads Type. Default is ADMOB
     *
     * @return
     */
    protected AdManager.AdsType getAdsType() {
        int value = sharedPreferences.getInt(KEY_ADS_NUMBER, AdManager.AdsType.ADMOB.ordinal());
        switch (value) {
            case 1:
                return AdManager.AdsType.ADMOB;
            default:
                return AdManager.AdsType.START_APP;
        }


    }

    /**
     * Store App
     *
     * @param value
     */
    protected void setAdsNumber(int value) {
        sharedPreferences.edit().putInt(KEY_ADS_NUMBER, value < 0 ? 0 : value).commit();
    }
}
