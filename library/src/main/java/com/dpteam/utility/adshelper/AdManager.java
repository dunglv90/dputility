package com.dpteam.utility.adshelper;

import android.app.Activity;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.startapp.android.publish.Ad;
import com.startapp.android.publish.AdEventListener;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Ads manager is used to load config from server and show StartApp or Admob ads.
 * <pre>
 * <br><b>Adding Permissions:</b>
 * {@literal <uses-permission android:name="android.permission.INTERNET"/>}
 * {@literal <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>}
 * {@literal <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>}
 * <br><b>Adding activity to show Ads </b>
 * {@literal<activity android:name="com.startapp.android.publish.list3d.List3DActivity" android:theme="@android:style/Theme" />}
 * {@literal<activity android:name="com.startapp.android.publish.OverlayActivity" android:theme="@android:style/Theme.Translucent" android:configChanges="orientation|keyboardHidden|screenSize" />}
 * {@literal <activity android:name="com.startapp.android.publish.FullScreenActivity" android:theme="@android:style/Theme" android:configChanges="orientation|keyboardHidden|screenSize" />}
 * {@literal <activity android:name="com.google.android.gms.ads.AdActivity"
 * android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize"
 * android:theme="@android:style/Theme.Translucent" />}
 * <br><b>Adding meta-data to use Google play service</b>
 * {@literal <meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version" />}
 * </pre>
 * <pre>
 * <br><b>How to use in 1st Activity:</b> (in other activity you should not call initAds function)
 * 1. Call static function: {@link #initAds(Activity, String, OnAdsConfigListener)} with start App App Id, listener when finish loading config.
 * To change default ads config link {@literal http://dpteams.com/ads_config/ads_config_1.txt} , use function {@link #setAdsConfigUrl(String)} before calling function initAds
 * <b>Note:</b>
 * you should create new instance of {@link #AdManager} inside {@link OnAdsConfigListener}, then load fullscreen or ads banner here.
 * <br>
 * <code>AdManager.initAds(this, ConfigAdsUrl, StartAppAdsId, new AdManager.OnAdsConfigListener() {
 *      public void onAdsConfigLoaded() {
 *      // local admanager to show ads when user open app
 *      AdManager adManager1 = new AdManager(MainActivity.this);
 *      adManager1.loadInterstitialAds(true, AdmobFullScreenId);
 *      // global adManager to load ads only and add banner. Fullscreen ads can
 *      //  be showed when exit app.
 *      adManager = new AdManager(MainActivity.this);
 *      adManager.loadInterstitialAds(false, AdmobFullScreenId);
 *      adManager.addBanners(bannerView, AdmobBannerId);
 *      }
 * });
 * </code>
 * 2. show exit app when user close app (press back button):
 * <code>super.onBackPressed();
 * adManager.showInterstitialAds();
 * </code>
 * </pre>
 */
public class AdManager {
    public enum AdsType {
        START_APP, ADMOB
    }

    private static final String TAG = "AdManager";

    private AdsType mAdsType;
    private static String url_ads_config = "http://dpteams.com/ads_config/ads_config_1.txt";
    private AdView adView;

    /**
     * Init ads to load config from Server. Should call before setting content view of activity
     *
     * @param activity
     * @param startApp_app_id
     * @param onAdsConfigListener : you should load or display ads from here.
     */
    public static void initAds(Activity activity, String startApp_app_id, OnAdsConfigListener onAdsConfigListener) {
        initStartApp(activity, startApp_app_id);
        loadConfigFromServer(activity, onAdsConfigListener);
    }

    /**
     * Change ads config link
     *
     * @param url
     * @return
     */
    public static void setAdsConfigUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            AdManager.url_ads_config = url;
        }
    }

    /**
     * Call it after #initAds function called.
     *
     * @param activity
     */
    public AdManager(Activity activity) {
        this.activity = activity;
        this.mAdsType = AdConfigPreferences.getInstance(activity).getAdsType();
        if (mAdsType == AdsType.START_APP) { // create start app ads
            mStartAppAds = new StartAppAd(activity);
        }
    }

    /**
     * Load interstitial Ads and show it after load if need
     *
     * @param showAfterLoad : should show this fullscreen ads after finished loading or not?
     */
    public void loadFullScreenAds(boolean showAfterLoad, String admob_interstitial_id) {
        Log.d(TAG, "Load fullscreen: " + mAdsType.name());
        if (mAdsType == AdsType.START_APP) {
            if (showAfterLoad) {
                showStartAppFullScreen();
            }
        } else if (mAdsType == AdsType.ADMOB) {
            initInterstitialAdMob(showAfterLoad, admob_interstitial_id);
        }
    }

    /**
     * Show loaded ads previous
     */
    public void showFullScreenAds() {
        if (mAdsType == AdsType.ADMOB) {
            showAdMobFullScreen();
        } else {
            showStartAppFullScreen();
        }
    }

    /**
     * Add and load banner view into view group
     *
     * @param view
     * @param admob_banner_id
     */
    public void addBanners(final ViewGroup view, String admob_banner_id) {
        if (this.mAdsType == AdsType.ADMOB) {
            adView = new AdView(this.activity);
            view.addView(adView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            view.setVisibility(View.GONE);
            adView.setAdSize(AdSize.SMART_BANNER);
            adView.setAdUnitId(admob_banner_id);
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    adView.setVisibility(View.VISIBLE);
                    view.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    adView.setVisibility(View.GONE);
                    adView.setVisibility(View.GONE);
                }
            });
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        }
    }


    /**
     * Load ads Config from specific Links
     *
     * @param context
     * @param onAdsConfigListener
     */
    private static void loadConfigFromServer(final Activity context, final OnAdsConfigListener onAdsConfigListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String data = getHttp(url_ads_config);
                    Log.d("loadConfigFromServer", "Data: " + data);
                    JSONObject jsonObject = new JSONObject(data);
                    // Load ads config and store in preferences
                    if (jsonObject.has(context.getPackageName())) {
                        int value = jsonObject.getInt(context.getPackageName());
                        AdConfigPreferences.getInstance(context).setAdsNumber(value);
                        Log.d(TAG, "Package: " + context.getPackageName() + ", value: " + value);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // run on Ui thread
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Fire ads config callback
                        if (onAdsConfigListener != null)
                            onAdsConfigListener.onAdsConfigLoaded();
                    }
                });

            }
        }).start();
    }

    /**
     * Get Http String from remote Url.
     *
     * @param url_links
     * @return
     */
    private static String getHttp(String url_links) {
        BufferedReader in = null;
        String data = null;
        try {
            URL url = new URL(url_links);
            URLConnection urlConnection = url.openConnection();
            in = new BufferedReader(new InputStreamReader((new BufferedInputStream(urlConnection.getInputStream()))));

            StringBuffer sb = new StringBuffer("");
            String l = "";
            while ((l = in.readLine()) != null) {
                sb.append(l);
            }
            in.close();
            data = sb.toString();
            return data;
        } catch (Exception e) {
            if (in != null) {
                try {
                    in.close();
                    return null;
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }

        return null;
    }

    /**
     * Listener to first ads config loaded from Our server
     */
    public interface OnAdsConfigListener {
        public void onAdsConfigLoaded();
    }


    // ======================================================================
    // Admobs
    // ======================================================================
    private InterstitialAd interstitialAd;
    private Activity activity;

    private void initInterstitialAdMob(final boolean showAfterLoaded, String admob_interstitial_id) {
        // Create an ad.
        interstitialAd = new InterstitialAd(activity);
        interstitialAd.setAdUnitId(admob_interstitial_id);
        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if (showAfterLoaded) {
                    showAdMobFullScreen();
                }
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                // request new Interstitial
//                if (interstitialAd != null) {
//                    AdRequest adRequest = new AdRequest.Builder().build();
                // Begin loading your interstitial
//                    interstitialAd.loadAd(adRequest);
//                }
            }
        });
        // Begin loading your interstitial
        interstitialAd.loadAd(adRequest);

    }


    /**
     * Invoke showAdMobFullScreen() when you are ready to display an
     * interstitial.
     */
    private void showAdMobFullScreen() {
        if (interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }

    public void onPause() {
        if (mAdsType == AdsType.ADMOB && adView != null) {
            adView.pause();
        }
    }

    public void onResume() {
        if (mAdsType == AdsType.ADMOB && adView != null) {
            adView.resume();
        }
    }

    public void onDestroy() {
        if (mAdsType == AdsType.ADMOB && adView != null) {
            adView.destroy();
        }
    }

    // ======================================================================
    // Start App Ads
    // ======================================================================
    private static final String START_APP_DEV_ID = "109106876";

    private StartAppAd mStartAppAds;

    /**
     * In your main activity, go to the OnCreate method and before calling
     * setContentView() call the static function:
     *
     * @param activity
     */
    private static void initStartApp(Activity activity, String startAppAppId) {
        StartAppSDK.init(activity, START_APP_DEV_ID, startAppAppId, false);
    }


    /**
     * Show Ads of {@link StartAppAd} when exit
     */
    private void showStartAppInterstialAds() {
        try {
            mStartAppAds.showAd(); // show the ad
            mStartAppAds.loadAd(); // load the next ad
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Show Ads of {@link StartAppAd} when start
     */
    private void showStartAppFullScreen() {
        if (mStartAppAds == null) {
            return;
        }
        if (!mStartAppAds.isReady()) {
            mStartAppAds.loadAd(new AdEventListener() {

                @Override
                public void onReceiveAd(Ad arg0) {
                    // Ko dung cai nay, nhieu luc ko show dc.
                    (new Handler()).postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            showStartAppInterstialAds();
                        }
                    }, 500);
                }

                @Override
                public void onFailedToReceiveAd(Ad arg0) {
                }
            });
        } else {
            showStartAppInterstialAds();
        }

    }
}
