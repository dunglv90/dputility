package com.dpteam.utility.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment extends Fragment {

    protected Activity context;

    /**
     * @return resource id of layout of fragment
     */
    abstract protected int getLayoutId();

    protected abstract void initializeViews(View view);

    protected abstract void setContentForView(View view);

    /**
     * Function only called when fragment is created when configuration changed
     *
     * @param savedInstanceState
     */
    protected void onOrientationChanged(Bundle savedInstanceState) {
        // Call when orientation changed
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            onOrientationChanged(savedInstanceState);
        }
        View view = inflater.inflate(getLayoutId(), container, false);
        initializeViews(view);
        setContentForView(view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.context = (Activity) context;
        }
    }
}
