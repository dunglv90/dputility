package com.dpteam.utility.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * This class is used for filtering adapter with String
 *
 * @param <T> : type of Object to show to view
 */
public abstract class BaseFilterableArrayAdapter<T> extends BaseArrayAdapter<T>
        implements Filterable {

    public BaseFilterableArrayAdapter(Activity context, int resItemId, List<T> objects) {
        super(context, resItemId, objects);
    }

    protected List<T> mBackupData;

    /**
     * Setup backup data for searching
     *
     * @param backupData
     */
    public void setmBackupData(List<T> backupData) {
        this.mBackupData = backupData;
    }

    /**
     * Get backup data to add or remove item from it
     *
     * @return
     */
    public List<T> getBackupData() {
        return this.mBackupData;
    }

    /**
     * Is this filterable ignore case? Now always return true
     *
     * @return
     */
    protected boolean isIgnoreCase() {
        return true;
    }

    /**
     * Get value to filter (example: name...)
     * <br> Should I change this fucntion to return list of condition?
     *
     * @param object
     * @return String or empty String (Make sure it's not null)
     */
    protected abstract String getFilterableString(T object);

    /**
     * Get comparator to sort adapter
     *
     * @return
     */
    protected Comparator<T> getSortComparator() {
        return null;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                if (mBackupData == null) {
                    throw new IllegalArgumentException("You must set Backup Data for this shit " +
                            "before performing filtering via functoin: setmBackupData(List<T>)");
                }
                FilterResults filterResults = new FilterResults();
                if (TextUtils.isEmpty(charSequence)) {
                    filterResults.values = mBackupData;
                } else {
                    String searchText = charSequence.toString().toLowerCase();
                    List<T> list = new ArrayList<>();
                    for (T object : mBackupData) {
                        if (getFilterableString(object).toLowerCase().contains(searchText)) {
                            list.add(object);
                        }
                    }
                    filterResults.values = list;
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                clear(); // clear adapter
                if (filterResults != null && filterResults.values != null) {
                    List<T> list = (List<T>) filterResults.values;
                    for (T object : list) {
                        add(object);
                    }
                }
                Comparator<T> comparator = getSortComparator();
                if (comparator != null) {
                    sort(comparator);
                }
                notifyDataSetChanged();
            }
        };
    }

//    public interface FilterableObject {
//        /**
//         * Return list of String for filter
//         *
//         * @return
//         */
//        public List<String> getFilterString();
//    }
}
