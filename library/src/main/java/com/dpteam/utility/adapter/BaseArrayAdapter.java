package com.dpteam.utility.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * To use this Adapter you must: create inner class implementing ViewHolderBase
 * class and return it on getViewHolder function.
 *
 * @param <T>
 * @author kingfisher.phuoc
 */
public abstract class BaseArrayAdapter<T> extends ArrayAdapter<T> {

    private int resId;
    private LayoutInflater mLayoutInflater;

    protected Activity mActivity;

    public BaseArrayAdapter(Activity context, int resItemId, List<T> objects) {
        super(context, resItemId, objects);
        this.resId = resItemId;
        mActivity = context;
        mLayoutInflater = mActivity.getLayoutInflater();
    }

    protected interface ViewHolderBase {

        public void findViews(View view);

        public void setData(int position);
    }

    /**
     * Must return child's ViewHolder
     *
     * @return
     */
    protected abstract ViewHolderBase getViewHolder();

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderBase viewHolder = null;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(resId, null);
            viewHolder = getViewHolder();
            viewHolder.findViews(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderBase) convertView.getTag();
        }
        viewHolder.setData(position);
        return convertView;
    }
}
