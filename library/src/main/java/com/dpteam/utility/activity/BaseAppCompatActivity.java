package com.dpteam.utility.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by DungLV on 14/5/2014.
 */
public abstract class BaseAppCompatActivity extends AppCompatActivity {
    public FragmentActivity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        initializeBeforeSetContent();
        this.setContentView(getActivityLayout());
        initializeViews();
        setContentForView();
    }

    protected abstract int getActivityLayout();

    protected abstract void initializeBeforeSetContent();

    protected abstract void initializeViews();

    protected abstract void setContentForView();
}
